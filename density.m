%Written to calculate SW density from T&S using the Millero and Poisson
%1981 density equation. This is the SW density equation used in Dickson et
%al. BP Guide for Ocean CO2 Measurements, Ch. 5
function [sample_density]=density(T,S)
% Convert from ITS-90 temperature scale to ITPS-68 temperature scale (eq. from Jones and Harris 1992)
% since the latter is the proper scale for the Millero and Poisson 1981 SW
% density equation. Assumes input T is already on the ITS-90 scale.
T68=(T-0.0002)/0.99975;

%Now perform density calculation according to Millero and Poisson 1981

density_SMOW=999.842594+((6.793952*10^-2)*T68)-((9.095290*10^-3)*(T68.^2))+((1.001685*10^-4)*T68.^3)-((1.120083*10^-6)*T68.^4)+((6.536332*10^-9)*T68.^5);
%Density equation for SMOW. This is the density equation listed in Dickson
%et al. 2007 BP Guide. Original citation is Millero and Poisson 1981.
A=(8.24493*10^-1)-((4.0899*10^-3)*T68)+((7.6438*10^-5)*T68.^2)-((8.2467*10^-7)*T68.^3)+((5.3875*10^-9)*T68.^4);
B=(-5.72466*10^-3)+((1.0227*10^-4)*T68)-((1.6546*10^-6)*T68.^2);
C_density=4.8314*10^-4;
%Constants also from Millero and Poisson 1981
sample_density=density_SMOW+A.*S+(B.*(S.^1.5))+(C_density*(S.^2));
end