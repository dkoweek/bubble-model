%% Written by David Koweek in Feb 2015 to generate ODEs for bubble plume model
% Note: input argument concentrations all in mmol/m3
%
% Additional parameterizations for bubble slip velocity and gas transfer velocity are found below and can be uncommented for use in model simulations 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function yprime=bubble_ODES(z,y, T, S, O2, N2, DIC, TA, Si, Phos)
        
        
        %Seawater properties
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        load static_bubble_model_parameters
        load dynamic_bubble_model_parameters
        rho=density(T, S); %water density (kg m^3)
        delta_P=rho*g*abs(z); %Pa
        Pa_to_atm=9.86923267e-6; %conversion factor
        delta_P=delta_P*Pa_to_atm; 
        P=P_surface+delta_P; %atm
        T_K=T+273.15;
        kv=SW_Kviscosity(T, 'C', S, 'ppt'); %m^2/s
        %Gas solubilities
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %O2
            %Bunsen solubility coefficients for O2 (from Weiss 1970)
            A_O2=[-58.3877, 85.8079, 23.8439];
            B_O2=[-0.034892, 0.015568, -0.0019387];
            Bunsen_O2=exp((A_O2(1)+(A_O2(2)*(100/T_K))+(A_O2(3)*(log(T_K/100))))+...
                (S*(B_O2(1)+(B_O2(2)*(T_K/100))+(B_O2(3)*((T_K/100).^2)))));
            S_O2=(Bunsen_O2/V_bar_ideal)*1e6; %mmol/m^3/atm

            %N2
            %Bunsen solubility coefficients
            A_N2=[-59.6274, 85.7761, 24.3696];
            B_N2=[-0.051580, 0.026329, -0.0037252];
            Bunsen_N2=exp((A_N2(1)+(A_N2(2)*(100/T_K))+(A_N2(3)*(log(T_K/100))))+...
                (S*(B_N2(1)+(B_N2(2)*(T_K/100))+(B_N2(3)*((T_K/100).^2)))));
            S_N2=(Bunsen_N2/V_bar_ideal)*1e6; %mmol/m^3/atm

            %CO2
            %Volumetric solubility function coefficients
            A_CO2=[-160.7333, 215.4152, 89.8920,-1.47759];
            B_CO2=[0.029941,-0.027455,0.0053407];
            F_CO2=exp((A_CO2(1)+(A_CO2(2)*(100/T_K))+(A_CO2(3)*(log(T_K/100)))+(A_CO2(4)*((T_K/100).^2)))+...
                (S*(B_CO2(1)+(B_CO2(2)*(T_K/100))+(B_CO2(3)*((T_K/100).^2)))));
            pH2O_to_P=exp(24.4543-67.4509*(100/T_K)-4.8489*log(T_K/100)-0.000544*S);
            S_CO2=(F_CO2/(P_surface-(pH2O_to_P*P_surface)))*1e6; %mmol/m^3/atm
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        pO2_bubble=(y(1)/(y(1)+y(2)))*P; %atm
        pN2_bubble=(y(2)/(y(1)+y(2)))*P; %atm
        pCO2_bubble=(y(3)/(y(1)+y(2)))*P; %atm
        m_O2=pO2_bubble*S_O2; %mmol/m^3
        m_N2=pN2_bubble*S_N2; %mmol/m^3
        V_g=((m_O2+m_N2)/P)*R*T_K; %unitless gas volume ratio (ignores volumetric contribution from CO2)
        r=((3*(sum(y(1:3))*R*T_K))/(4*pi*N*P))^(1/3); %m: from ideal gas law (P_b*Q=sum(n)*R*T)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
        
        %Bubble slip velocity (m/s)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
            %Eq. 38 in Woolf (1993), Bubbles and the air-sea transfer velocity of gases, Atmosphere-Ocean, 31 p. 517-540
              w_b=0.172*r^1.28.*g^0.76*kv^-0.56; %m/s
              if w_b>=0.25 
                  w_b=0.25;
              end
%              
%             %Eq. 16 in Leifer and Patro (2002), The bubble mechanism for methane transport from the shallow sea bed to the surface: 
%             %...A review and sensitivity study, Continental Shelf Research, 22 p. 2409-2428
%             w_b=276*(r*100)-1648*((r*100).^2)+4882*((r*100).^3)-7429*((r*100).^4)+5618*((r*100).^5)-1670*((r*100).^6); %cm/s
%             w_b=w_b/100; %m/s
%             
%             %Eq. 13 in Woolf and Thorpe (1991), Bubbles and the air-sea exchange of gases in near-saturation conditions, Journal of
%             %...Marine Research, 49 p. 435-466
%             shape_factor=10.82*(kv^2)*(g^-1)*(r^-3);
%             w_b=(2*(r^2)*g)/(9*kv)*((((shape_factor.^2)+2*shape_factor).^0.5)-shape_factor); %m/s
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Individual bubble gas transfer velocity (m/s)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Schmidt numbers for O2, N2, and CO2 (coefficients from Sarmiento and Gruber (2006) Ocean Biogeochemical Dynamics Table 3.3.1)
        A_Sc=[1638; 2206.1; 2073.1];
        B_Sc=[81.83; 144.86; 125.62];
        C_Sc=[1.483; 4.5413; 3.6276];
        D_Sc=[0.008004; 0.056988; 0.043219];
        Sc_O2=A_Sc(1)-(B_Sc(1)*T)+(C_Sc(1)*T^2)-(D_Sc(1)*T^3);
        Sc_N2=A_Sc(2)-(B_Sc(2)*T)+(C_Sc(2)*T^2)-(D_Sc(2)*T^3);
        Sc_CO2=A_Sc(3)-(B_Sc(3)*T)+(C_Sc(3)*T^2)-(D_Sc(3)*T^3);
        D_O2=kv/Sc_O2; %m^2/s
        D_N2=kv/Sc_N2; %m^2/s
        D_CO2=kv/Sc_CO2; %m^2/s
        Re=2*r*w_b/kv; %Reynolds number (defined in Memery and Merlivat 1985)

%             %Eq. 5 in Memery and Merlivat (1985) Modeling of gas flux through bubbles at the air-water interface, Tellus B, 37B, p. 272-285
              k_O2=8*((D_O2^2*w_b/r^2)^(1/3));
              k_N2=8*((D_N2^2*w_b/r^2)^(1/3));
              k_CO2=8*((D_CO2^2*w_b/r^2)^(1/3));
%             
%             %Eq. 22 in Leifer and Patro (2002) (from Clift 1978), The bubble mechanism for methane transport from the shallow sea 
%             %...bed to the surface: A review and sensitivity study, Continental Shelf Research, 22 p. 2409-2428
%             k_O2=0.42*(g^0.3)*(Sc_O2.^(-2/3))*kv^0.4*r^-0.1;
%             k_N2=0.42*(g^0.3)*(Sc_N2.^(-2/3))*kv^0.4*r^-0.1;
%             k_CO2=0.42*(g^0.3)*(Sc_CO2.^(-2/3))*kv^0.4*r^-0.1;
%             
%             %Eq. 16 in Woolf and Thorpe (1991), Bubbles and the air-sea exchange of gases in near-saturation conditions, Journal of
%             %...Marine Research, 49 p. 435-466
%                 %Peclet numbers
%                 Pe_O2=r*w_b/D_O2;
%                 Pe_N2=r*w_b/D_N2;
%                 Pe_CO2=r*w_b/D_CO2;
% 
%                 %Nusselt numbers (use Eq. 20 in Woolf and Thorpe since Re>>8)
%                 Nu_O2=0.45*Re^(1/6)*Pe_O2^(1/3);
%                 Nu_N2=0.45*Re^(1/6)*Pe_N2^(1/3);
%                 Nu_CO2=0.45*Re^(1/6)*Pe_CO2^(1/3);
%             
%              k_O2=D_O2*Nu_O2/r;
%              k_N2=D_N2*Nu_N2/r;
%              k_CO2=D_CO2*Nu_CO2/r;
% 
%             %Eq.9 in Tsuchiya et al. (1997) Absorption dynamics of CO2 bubbles in a pressurized liquid flowing downward and its
%             %...simulation in seawater, Chemical Engineering Science, 52(21-22) p. 4199-4126
%                  surfactant_factor=10.^(0.5*(tanh(3.9*(log10(2*(r*1000)/0.87)))-1)); %unitless ratio
%             k_O2=2/sqrt(pi)*((1-(2.89*(Re^-0.5)))*(D_O2*surfactant_factor*w_b/r)).^0.5;
%             k_N2=2/sqrt(pi)*((1-(2.89*(Re^-0.5)))*(D_N2*surfactant_factor*w_b/r)).^0.5;
%             k_CO2=2/sqrt(pi)*((1-(2.89*(Re^-0.5)))*(D_CO2*surfactant_factor*w_b/r)).^0.5;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Use most recent aqueous phase concentrations to establish concentration gradients
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        O2_aq=O2(end); %mmol/m^3
        N2_aq=N2(end); %mmol/m^3
        DIC_aq=DIC(end); %mmol/m^3
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Carbonate system equilibration
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        C=CO2SYS(TA/(rho/1000), DIC_aq/(rho/1000), 1, 2 , S, T, T, P, P, Si/(rho/1000), Phos/(rho/1000), 1, 4, 1);
        pCO2_aq=C(4)*1e-6; %atm
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Concatenate newest values for dynamic model parameters onto vector of values for each parameter
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        w_b_all=[w_b_all w_b];
        r_all=[r_all r];
        V_g_all=[V_g_all V_g];
        z_all=[z_all z];
        save('dynamic_bubble_model_parameters',  '*_all*')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Coupled, non-linear ODEs to describe changes in gaseous molar flow rates (Eq. 11 in McGinnis and Little (2002) Predicting
        %                   diffused-bubble oxygen transfer rate using the discrete-bubble model, Water Research, 36, p. 4627-4635)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        yprime=zeros(4,1);
        yprime(1)=-((4*pi*(r.^2)*N)/w_b)*(k_O2*(m_O2-O2_aq)); %dG_O2/dz (mmol/m/s)
        yprime(2)=-((4*pi*(r.^2)*N)/w_b)*(k_N2*(m_N2-N2_aq)); %dG_N2/dz (mmol/m/s)
        yprime(3)=-((4*pi*(r.^2)*N)/w_b)*(k_CO2*S_CO2*(pCO2_bubble-pCO2_aq)); %dG_CO2/dz (mmol/m/s)
        yprime(4)=1/w_b; %dtime/dz (easy way to keep track of time for each simulation) (s/m)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end