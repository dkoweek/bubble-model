%% Model results analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_interp_to_t=interp1(time_parameter, T,t); %interpolate temperature time series to model time step

model_spinup=1; %days
actual_start=find(t>model_spinup,1);
t=t(actual_start:end); C=C(actual_start:end,:); T_interp_to_t=T_interp_to_t(actual_start:end);

% Solve carbon system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C_system_bubbling=CO2SYS(C(:,1), C(:,2), ones(length(C),1), 2*ones(length(C),1), S*ones(length(C),1), T_interp_to_t, T_interp_to_t,...
                    zeros(length(C),1), zeros(length(C),1), Si*ones(length(C),1), Phos*ones(length(C),1), ones(length(C),1), 4*ones(length(C),1),...
                    ones(length(C),1));
pH_bubbling=C_system_bubbling(:,3);
pCO2_bubbling=C_system_bubbling(:,4);
omega_bubbling=C_system_bubbling(:,16);
                
                
C_system_control=CO2SYS(C(:,1), C(:,4), ones(length(C),1), 2*ones(length(C),1), S*ones(length(C),1), T_interp_to_t, T_interp_to_t,...
                    zeros(length(C),1), zeros(length(C),1), Si*ones(length(C),1), Phos*ones(length(C),1), ones(length(C),1), 4*ones(length(C),1),...
                    ones(length(C),1)); 
                
pH_control=C_system_control(:,3);
pCO2_control=C_system_control(:,4);
omega_control=C_system_control(:,16);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Subsidy analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pH_subsidy=trapz(t, pH_bubbling)-trapz(t, pH_control); %pH*days
pH_subsidy=pH_subsidy*24; %pH*hours

pCO2_subsidy=trapz(t, pCO2_bubbling)-trapz(t, pCO2_control); %uatm*days
pCO2_subsidy=pCO2_subsidy*24; %uatm*hours

omega_subsidy=trapz(t, omega_bubbling)-trapz(t, omega_control); %omega*days
omega_subsidy=omega_subsidy*24; %omega*hours
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Severity analysis (based on Hauri et al. 2013 GRL)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%pH
pH_threshold=7.8; %(total scale)
    pH_bubbling_intensity=pH_bubbling-pH_threshold;
    pH_control_intensity=pH_control-pH_threshold;
    
    %Exchange all values above threshold for 0 in order to only integrate values below threshold
    pH_bubbling_intensity(pH_bubbling_intensity>0)=0;
    pH_control_intensity(pH_control_intensity>0)=0;
    
    %Calculate Severity=IxD
    pH_bubbling_severity=trapz(t, pH_bubbling_intensity); %pH*days
    pH_bubbling_severity=pH_bubbling_severity*24; %pH*hours
    pH_control_severity=trapz(t, pH_control_intensity); %pH*days
    pH_control_severity=pH_control_severity*24; %pH*hours

    pH_severity_subsidy=pH_bubbling_severity-pH_control_severity; %pH*hours

%pCO2
pCO2_threshold=900; %uatm
    pCO2_bubbling_intensity=pCO2_bubbling-pCO2_threshold;
    pCO2_control_intensity=pCO2_control-pCO2_threshold;
    
    %Exchange all values above threshold for 0 in order to only integrate values below threshold
    pCO2_bubbling_intensity(pCO2_bubbling_intensity>0)=0;
    pCO2_control_intensity(pCO2_control_intensity>0)=0;
    
    %Calculate Severity=IxD
    pCO2_bubbling_severity=trapz(t, pCO2_bubbling_intensity); %uatm*days
    pCO2_bubbling_severity=pCO2_bubbling_severity*24; %uatm*hours
    pCO2_control_severity=trapz(t, pCO2_control_intensity); %uatm*days
    pCO2_control_severity=pCO2_control_severity*24; %uatm*hours

    pCO2_severity_subsidy=pCO2_bubbling_severity-pCO2_control_severity; %uatm*hours

%Omega_Ar
omega_threshold=2; 
    omega_bubbling_intensity=omega_bubbling-omega_threshold;
    omega_control_intensity=omega_control-omega_threshold;
    
    %Exchange all values above threshold for 0 in order to only integrate values below threshold
    omega_bubbling_intensity(omega_bubbling_intensity>0)=0;
    omega_control_intensity(omega_control_intensity>0)=0;
    
    %Calculate Severity=IxD
    omega_bubbling_severity=trapz(t, omega_bubbling_intensity); %omega*days
    omega_bubbling_severity=omega_bubbling_severity*24; %omega*hours
    omega_control_severity=trapz(t, omega_control_intensity); %omega*days
    omega_control_severity=omega_control_severity*24; %omega_hours 

    omega_severity_subsidy=omega_bubbling_severity-omega_control_severity; %omega*hours