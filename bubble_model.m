%% Bubble plume model written by David Koweek in Feb 2015 to explore bubble plume gas transfer.
% Model used in Koweek et al. "Bubble stripping as a tool to reduce high dissolved CO2 in coastal marine ecosystems"
% Input arguments:
% Q_per_m2: compressed air flow rate released from source (L/m^2/s)
% b: bubble plume radius (m)
% P_gas: pressure of compressed gas once released from compressed gas source, such as a blower (atm). 
%        Must set P_gas high enough to overcome hydrostatic pressure at the depth of the diffusers. 
% time_duration: time of bubbling (i.e. model domain) (hrs)
% lateral velocity: lateral current moving through bubble plume (m/s)
% O2_a, N2_a, DIC_a: ambient concentrations of O2, N2, and DIC advecting into bubble plume (mmol/m^3)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function bubble_model(Q_per_m2,b,P_gas, time_duration, lateral_velocity, O2_a, N2_a, DIC_a)

%Initialize model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
timestamp=datestr(now, 'yyyymmddHHMMSS');
load Standard_Conditions %Load initial conditions here. Can modify initial conditions file for different starting conditions
elapsed_time=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


i=1; %Begin model iterations

while sum(elapsed_time)<time_duration
    
    
    %Initialize model
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    IC=bubble_model_initialization(Q_per_m2,b, P_gas, T, S, bottom_depth);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    %Define model domain
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    depth_span=[-bottom_depth,0]; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %Execute model
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [z,y]=ode45(@(z,y) bubble_ODES(z,y, T, S, O2, N2, DIC, TA, Si, Phos), depth_span, IC);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    %Model Results
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
        
        %Recall static and dynamic model parameters. Interpolate across model domain for dynamic model parameters.
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        load static_bubble_model_parameters
        load dynamic_bubble_model_parameters
        [~,unique_depths,~]=unique(z_all);
        w_bubble{i}=interp1(z_all(unique_depths), w_b_all(unique_depths), z);
        r_bubble{i}=interp1(z_all(unique_depths), r_all(unique_depths), z);
        gas_fraction{i}=interp1(z_all(unique_depths), V_g_all(unique_depths), z);
        Z{i}=z; %save model domain from each run for later model diagnostics
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Calculate molar exchange rates for O2, N2, and CO2
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        delta_m_O2_int=trapz(y(:,4), (y(:,1)-y(1,1))); %mmol
        delta_m_N2_int=trapz(y(:,4), (y(:,2)-y(1,2))); %mmol
        delta_m_CO2_int=trapz(y(:,4), (y(:,3)-y(1,3))); %mmol
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
        %Advective effects on aquous phase concentrations
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Q_adv=((pi^2)*b*bottom_depth*lateral_velocity)/4; %m^3/s 
        delta_O2_adv=-Q_adv*(O2(i)-O2_a)*y(end,4); %mmol
        delta_N2_adv=-Q_adv*(N2(i)-N2_a)*y(end,4); %mmol
        delta_DIC_adv=-Q_adv*(DIC(i)-DIC_a)*y(end,4); %mmol
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        
        %Bulk gas exchange at the air/sea interface due to turbulence in the water column
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
        epsilon=8.3e-6; %TKE dissipation rate (m^2/s^3)
        rho=density(T,S); %kg/m^3
        kv=SW_Kviscosity(T, 'C', S, 'ppt'); %kinematic viscosity of water (m^2/s)
        P_surface=1; %atm
        T_K=T+273.15; %K
        %Schmidt coefficients
            A_Sc=[1638; 2206.1; 2073.1];
            B_Sc=[81.83; 144.86; 125.62];
            C_Sc=[1.483; 4.5413; 3.6276];
            D_Sc=[0.008004; 0.056988; 0.043219];
        Sc_O2=A_Sc(1)-(B_Sc(1)*T)+(C_Sc(1)*T^2)-(D_Sc(1)*T^3);
        Sc_N2=A_Sc(2)-(B_Sc(2)*T)+(C_Sc(2)*T^2)-(D_Sc(2)*T^3);
        Sc_CO2=A_Sc(3)-(B_Sc(3)*T)+(C_Sc(3)*T^2)-(D_Sc(3)*T^3);
            %O2
                %Bunsen solubility coefficients for O2 (from Weiss 1970)
                    A_O2=[-58.3877, 85.8079, 23.8439];
                    B_O2=[-0.034892, 0.015568, -0.0019387];
                    Bunsen_O2=exp((A_O2(1)+(A_O2(2)*(100/T_K))+(A_O2(3)*(log(T_K/100))))+...
                        (S*(B_O2(1)+(B_O2(2)*(T_K/100))+(B_O2(3)*((T_K/100).^2)))));
                S_O2=(Bunsen_O2/V_bar_ideal)*1e6; %mmol/m^3/atm
            x_O2_atm=.20946; %mole fraction of O2 in the atmosphere
            p_O2_atm=x_O2_atm*P_surface; %atm
            k_O2_turb=0.419*(Sc_O2^-0.5)*(epsilon*kv)^0.25; %m/s
            k_O2_turb=k_O2_turb*3600; %m/hr
            O2_bulk_flux_rate=k_O2_turb*S_O2*(p_O2_atm-(O2(i)/S_O2))/bottom_depth; %mmol/m^3/hr
            O2_bulk_flux=O2_bulk_flux_rate*(y(end,4)/3600); %mmol/m^3
            %N2
                %Bunsen solubility coefficients
                    A_N2=[-59.6274, 85.7761, 24.3696];
                    B_N2=[-0.051580, 0.026329, -0.0037252];
                    Bunsen_N2=exp((A_N2(1)+(A_N2(2)*(100/T_K))+(A_N2(3)*(log(T_K/100))))+...
                        (S*(B_N2(1)+(B_N2(2)*(T_K/100))+(B_N2(3)*((T_K/100).^2)))));
                S_N2=(Bunsen_N2/V_bar_ideal)*1e6; %mmol/m^3/atm
            x_N2_atm=.78084; %mole fraction of N2 in the atmosphere
            p_N2_atm=x_N2_atm*P_surface; %atm
            k_N2_turb=0.419*(Sc_N2^-0.5)*(epsilon*kv)^0.25; %m/s
            k_N2_turb=k_N2_turb*3600; %m/hr
            N2_bulk_flux_rate=k_N2_turb*S_N2*(p_N2_atm-(N2(i)/S_N2))/bottom_depth; %mmol/m^3/hr
            N2_bulk_flux=N2_bulk_flux_rate*(y(end,4)/3600); %mmol/m^3    
            %CO2
                %Volumetric solubility function coefficients
                    A_CO2=[-160.7333, 215.4152, 89.8920,-1.47759];
                    B_CO2=[0.029941,-0.027455,0.0053407];
                    F_CO2=exp((A_CO2(1)+(A_CO2(2)*(100/T_K))+(A_CO2(3)*(log(T_K/100)))+(A_CO2(4)*((T_K/100).^2)))+...
                        (S*(B_CO2(1)+(B_CO2(2)*(T_K/100))+(B_CO2(3)*((T_K/100).^2)))));
                    pH2O_to_P=exp(24.4543-67.4509*(100/T_K)-4.8489*log(T_K/100)-0.000544*S);
                S_CO2=(F_CO2/(P_surface-(pH2O_to_P*P_surface)))*1e6; %mmol/m^3/atm
            C_sys=CO2SYS(TA/(rho/1000), DIC(i)/(rho/1000), 1, 2, S, T, T, 0, 0, Si, Phos, 1, 4, 1);
            pCO2_sw=C_sys(4)*1e-6; %atm
            pCO2_atm=400e-6; %atm
            k_CO2_turb=0.419*(Sc_CO2^-0.5)*(epsilon*kv)^0.25; %m/s
            k_CO2_turb=k_CO2_turb*3600; %m/hr
            CO2_bulk_flux_rate=k_CO2_turb*S_CO2*(pCO2_sw-pCO2_atm)/bottom_depth; %mmol/m^3/hr
            CO2_bulk_flux=CO2_bulk_flux_rate*(y(end,4)/3600); %mmol/m^3
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
        
        %Adjust aqueous phase concentrations based on gaseous phase changes in bubbles (subtract since addition in aqueous phase means loss in aqueous phase),
        %advective effects, and bulk transfer from turbulence in the water
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Volume=pi*b^2*bottom_depth; %m^3
        O2(i+1)=O2(i)-(delta_m_O2_int/Volume)+(delta_O2_adv/Volume)+O2_bulk_flux; %mmol/m^3
        N2(i+1)=N2(i)-(delta_m_N2_int/Volume)+(delta_N2_adv/Volume)+N2_bulk_flux;
        DIC(i+1)=DIC(i)-(delta_m_CO2_int/Volume)+(delta_DIC_adv/Volume)-CO2_bulk_flux; 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Calculate elapsed time, display updated progress, and save newest model iteration
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elapsed_time(i)=y(end,4)/3600; %hr
        disp(strcat('total_time_elapsed=', num2str(sum(elapsed_time)), ' hours'))
        filename=strcat('bubble_model_results_', timestamp);
        save(filename)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    i=i+1; %advance to next iteration

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end