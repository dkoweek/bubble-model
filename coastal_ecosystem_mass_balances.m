%% Written by David Koweek (2015) to solve the right-hand-side of a set of coupled bubbling-ecosystem mass balance ODEs
% Users must MANUALLY specify threshold-based or time-based bubbling. To
% specify threshold-based bubbling, uncomment lines 23-26 ("Threshold-based bubbling" section)
% PRIOR to running subsidy_model.m and set threshold accordingly. To run time-based bubbling, set the start and stop time
% in subsidy_model.m and ensure that lines 23-26 ARE COMMENTED prior to model execution.
function [C_prime,Q_per_m2, delta_CO2_bubble]=coastal_ecosystem_mass_balances(t,C, S, Si, Phos, offshore_water, CO2_system_off, time_parameter, NCP, NCC, R_PQ, R_TA_NCP, h, c_mixing, b, T, gas_flow)


%Interpolate time-dependent model parameters to current time step in model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NCC=interp1(time_parameter, NCC, t); %mmol/m2/d
NCP=interp1(time_parameter, NCP, t); %mmol/m2/d
h=interp1(time_parameter, h, t); %m
Q_per_m2=interp1(time_parameter, gas_flow,t); %L/m^2/s
c_mixing=interp1(time_parameter, c_mixing, t); %d^-1
T=interp1(time_parameter, T, t); %deg C
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rho=density(T,S); %kg/m^3

%Threshold-based bubbling-(Comment for time-based gas flow profiles. Uncomment and set bubbling criteria here for threshold-based gas flow profiles)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CO2_system_bubbling=CO2SYS(C(1)*1000, C(2)*1000, 1, 2, S, T, T, 0, 0, Si, Phos, 1, 4, 1);
pH=CO2_system_bubbling(3);
start_bubbling=pH<(CO2_system_off(3)-0.3); %Initiate bubble at 0.3 pH units below offshore pH
Q_per_m2=max(gas_flow)*start_bubbling; %gas will be fully on if pH is below threshold value and gas will not be on if pH is above threshold value
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Bubble-induced gas flux
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
bubble_IC=bubble_model_initialization(Q_per_m2,b,1.3, T,S,h); %Gas pressure set at fixed value of 1.3atm here (same value used for other.. 
                                                                %model experiments in the paper), but is adjustable for interested users. 
bubble_model_depth_domain=[-h 0];
[z,y]=ode45(@(z,y) bubble_ODES(z,y, T, S, C(3)*rho, C(6)*rho, C(2)*rho, C(1)*rho, Si*(rho/1000), Phos*(rho/1000)), bubble_model_depth_domain, bubble_IC);
 
    delta_m_O2_int=trapz(y(:,4), (y(:,1)-y(1,1))); %mmol
    delta_m_N2_int=trapz(y(:,4), (y(:,2)-y(1,2))); %mmol
    delta_m_CO2_int=trapz(y(:,4), (y(:,3)-y(1,3))); %mmol

    delta_O2_bubble=-delta_m_O2_int/(y(end,4)/86400)/(pi*b^2*h); %mmol/m^3/d
    delta_N2_bubble=-delta_m_N2_int/(y(end,4)/86400)/(pi*b^2*h); %mmol/m^3/d
    delta_CO2_bubble=-delta_m_CO2_int/(y(end,4)/86400)/(pi*b^2*h); %mmol/m^3/d

    delta_O2_bubble(isnan(delta_O2_bubble))=0;
    delta_N2_bubble(isnan(delta_N2_bubble))=0;
    delta_CO2_bubble(isnan(delta_CO2_bubble))=0;   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%ODEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C_prime=zeros(6,1);
C_prime(1)=(-2*NCC+R_TA_NCP*NCP)/(rho*h)-c_mixing*((C(1)-offshore_water(1))/2); %mmol/kg/d
C_prime(2)=((-NCP-NCC)/(rho*h))+(delta_CO2_bubble/rho)-c_mixing*((C(2)-offshore_water(2))/2); %DIC mass balance with bubbling
C_prime(3)=((R_PQ*NCP)/(rho*h))+(delta_O2_bubble/rho)-c_mixing*((C(3)-offshore_water(3))/2); %O2 mass balance with bubbling
C_prime(4)=(-NCP-NCC)/(rho*h)-c_mixing*((C(4)-offshore_water(2))/2); %DIC mass balance-no bubbling (control)
C_prime(5)=(R_PQ*NCP)/(rho*h)-c_mixing*((C(5)-offshore_water(3))/2); %O2 mass balance-no bubbling (control)
C_prime(6)=(delta_N2_bubble/rho)-c_mixing*((C(6)-offshore_water(4))/2); %N2 mass balance-no biological activity, just supersaturation from bubbling
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(strcat('total_elapsed_time=', num2str(t), ' days'));
end