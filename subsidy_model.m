%% Written by David Koweek on 19 June 2015 to simulate benefits of strategic bubbling as a geochemical engineering tool
% Input arguments:
% present_or_future: value of 1 will use present-day seawater conditions. Any other value will use end-of-century seawater conditions for model simulations
% Q_per_m2: Compressed air delivery rate (L/m^2/s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function subsidy_model(present_or_future, Q_per_m2)
%Set present_or_future=1 in order to specify present-day conditions. Otherwise, model will run at future seawater conditions
%Q_per_m2-specify gas flow rate when bubbling is turned on (L/m^2/s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Set initial conditions and offshore water concentrations-comment one set of conditions during model initialization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S=35; 
Si=3; Phos=1; %umol/kg

if present_or_future==1
%Modern Day (pH~8.1)
    %Initial conditions
        N2_0=415.4; %umol/kg (saturation at T=20, S=35)
        TA_0=2310; %umol/kg
        DIC_0=2150; %umol/kg
        O2_0=100; %umol/kg
    IC=[TA_0, DIC_0, O2_0, DIC_0, O2_0, N2_0]/1000; %move from umol/kg to mmol/kg

    %offshore water
        T_mean=20;
        TA_off=2300; %umol/kg
        DIC_off=2000; %umol/kg
        O2_off=224.9; %umol/kg (saturation at T=20, S=35)
        N2_off=415.4; %umol/kg (saturation at T=20, S=35)
    offshore_water=[TA_off, DIC_off, O2_off, N2_off]/1000; %move from umol/kg to mmol/kg
    CO2_system_off=CO2SYS(TA_off, DIC_off, 1, 2, S, T_mean, T_mean, 0,0, Si, Phos, 1,4,1);
else 
%End of century (delta SST~ +2.75, pH~7.8)
    %Initial conditions
        N2_0=408.6; %umol/kg (saturation at T=22.75, S=35)
        TA_0=2330; %umol/kg
        DIC_0=2250; %umol/kg
        O2_0=100; %umol/kg
    IC=[TA_0, DIC_0, O2_0, DIC_0, O2_0, N2_0]/1000; %move from umol/kg to mmol/kg

    %offshore water
        T_mean=22.75;
        TA_off=2300; %umol/kg
        DIC_off=2140; %umol/kg
        O2_off=220.1; %umol/kg (saturation at T=22.75, S=35)
        N2_off=408.6; %umol/kg (saturation at T=22.75, S=35)
    offshore_water=[TA_off, DIC_off, O2_off, N2_off]/1000; %move from umol/kg to mmol/kg
    CO2_system_off=CO2SYS(TA_off, DIC_off, 1, 2, S, T_mean, T_mean, 0,0, Si, Phos, 1,4,1);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Set model time span
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tspan=[0 8]; %days (1 day of model spin up before using results from 1 week)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Define time-dependent model parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
time_parameter=linspace(tspan(1),tspan(2), 240*(tspan(2)-tspan(1))); %time vector for time-dependent model parameters
dt=time_parameter(2)-time_parameter(1);
hr=round((1/24)/dt); %indices length corresponding to 1 hour

    %Metabolic rates
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    GPP_max=110*24; %mmol/m^2/hr -> mmol/m^2/d
    GPP=GPP_max*(sin(pi*time_parameter).^4); %mmol/m^2/d (light-based GPP parameterization modified from Cyronak et al. 2014- GRL)
    GPP_int=trapz(time_parameter, GPP); %mmol/m^2
    R_int=GPP_int; %Integrated GPP=integrated R (fair assumption over short time scales ~ 1 week)
    R=R_int/(tspan(2)-tspan(1)); %mmol/m^2/d
    NCP=GPP-R; %mmol/m^2/d
    R_PQ=1; %photosynthetic quotient (delta_O2/delta_CO2) associated with NCP 

    NCC_2_NCP=0.1; %ratio of inorganic to organic C metabolism
    NCC=NCC_2_NCP*NCP; %mmol/m^2/d
    
    R_TA_NCP=16/117; %changes in TA from NCP
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %Water Depth
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tidal_range=1; %meters
    depth_mean=2; %meters
    tidal_period=12.42/24; %days
    tidal_phase_shift=0; %phase shift for water depth (in hours)
    h=tidal_range/2*sin((2*pi/tidal_period)*(time_parameter-(tidal_phase_shift/24)))+depth_mean; %h=tidal_amplitude*sin(4pi*(t-phi_tidal))+mean_water_depth
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %Mixing coefficient
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    b=50; %plume radius (m)
    u_max=0.05; %maximum lateral velocity through bubble plume (m/s)
    allow_flow=(h-min(h)/(max(h)-min(h))>=0.75); %set water depth over which to allow flow
    u=u_max.*(h/max(h)).*allow_flow; %m/s
    tau=(4*b)./(pi*u); %residence time through bubble plume in a uniform field (seconds)
    tau=tau/86400; %seconds->days
    c_mixing=1./tau; %d^-1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %Water Temperature
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T_range=4; %deg C
    T_phase_shift=2; %Temperature phase shift (in hours)
    T_no_mixing=-T_range/2*cos(2*pi()*(time_parameter-(T_phase_shift/24)))+T_mean; %T=T_amplitude*cos(2pi*(t-phi_T))+T_mean (solar radiation only)
    dTdt_mixing=-c_mixing.*(T_no_mixing-T_mean)/2; %mixing term to account for advective heat fluxes
    dT_mixing=dTdt_mixing*dt; %dT=dTdt*dt
    T=T_no_mixing+dT_mixing;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %Compressed gas flow-time-based profile. Profile will be ignored if threshold-based bubbling is activated, but ODE solver will use Q_per_m2 (as maximum value of 'gas flow') 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    bubbling_start=3; %hour of the day to start bubbling
    bubbling_stop=5; %hour of the day to end bubbling
    flow_on=time_parameter-floor(time_parameter)>bubbling_start/24 & time_parameter-floor(time_parameter)<bubbling_stop/24;
    gas_flow=Q_per_m2*flow_on;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Solve subsidies model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
options=odeset('abstol', 1e-3); %1 umol/kg precision
[t,C]=ode23s(@(t,C) coastal_ecosystem_mass_balances(t,C, S, Si, Phos, offshore_water, CO2_system_off, time_parameter, NCP, NCC, R_PQ, R_TA_NCP, h, c_mixing, b, T, gas_flow), tspan, IC, options); %Need to use stiff ODE solver
    C=C*1000; %convert from mmol/kg to umol/kg
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Save model results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datestring=datestr(now, 'yyyymmddHHMMSS');
save(strcat('bubble_subsidy_analysis_', datestring))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end